import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';

class PhotoScreen extends StatelessWidget {
  final String url;
  final File? file;

  const PhotoScreen({
    super.key,
    this.url = '',
    this.file,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.greyColor,
        leadingWidth: 100,
        leading: GestureDetector(
          onTap: getItService.navigatorService.onBack,
          child: Container(
            color: Colors.transparent,
            child: const Row(
              children: [
                SizedBox(width: 10),
                Icon(
                  Icons.arrow_back_ios,
                  color: AppColors.secondaryColor,
                ),
                Text(
                  'Назад',
                  style: TextStyle(
                    color: AppColors.secondaryColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: InteractiveViewer(
        child: Center(
          child: file == null
              ? CachedNetworkImage(
                  imageUrl: url,
                  placeholder: (_, __) => const Center(child: CircularProgressIndicator()),
                  errorWidget: (_, __, ___) => Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.5,
                    color: Colors.transparent,
                    child: const Icon(
                      Icons.image_not_supported_outlined,
                      size: 50,
                    ),
                  ),
                )
              : Image.file(file!),
        ),
      ),
    );
  }
}
