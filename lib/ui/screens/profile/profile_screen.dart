// ignore_for_file: unused_element

import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:flutter_dsg/ui/screens/profile/change_profile_screen.dart';
import 'package:flutter_dsg/ui/screens/profile/widgets/avatar.dart';
import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/profile/states.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with SingleTickerProviderStateMixin {
  late Store<AppState> store;
  final authInteractor = getItService.authInteractor;

  @override
  void initState() {
    super.initState();
    store = StoreProvider.of<AppState>(context, listen: false);
    if (store.state.profileState.profile == null) store.dispatch(LoadProfileAction());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Аккаунт'),
        actions: [
          IconButton(
            onPressed: authInteractor.logOut,
            icon: const Icon(
              Icons.logout,
              color: AppColors.dangerColor,
            ),
          ),
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1,
              color: Colors.grey,
            ),
          ),
        ),
        child: StoreConnector<AppState, ProfileState>(
          converter: (store) => store.state.profileState,
          builder: (BuildContext context, ProfileState state) {
            if (state.isLoading) {
              return const Center(
                child: CircularProgressIndicator(
                  color: AppColors.secondaryColor,
                ),
              );
            }
            if (state.isLoading) {
              return const Center(
                child: CircularProgressIndicator(
                  color: AppColors.secondaryColor,
                ),
              );
            }
            return ListView(
              children: [
                const SizedBox(height: 24),
                Avatar(avatar: state.profile!.avatar),
                const SizedBox(height: 12),
                Text(
                  'email@gmail.com',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.labelSmall!.copyWith(color: Colors.black54),
                ),
                const SizedBox(height: 24),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(13),
                    ),
                  ),
                  child: Column(
                    children: [
                      _Button(
                        title: 'Имя',
                        value: state.profile!.firstName,
                        onTap: () => getItService.navigatorService.onChangeProfile(
                          type: TypeChangeProfile.firstName,
                        ),
                      ),
                      const Divider(
                        thickness: 0.5,
                        height: 1,
                        color: Colors.grey,
                      ),
                      _Button(
                        title: 'Фамилия',
                        value: state.profile!.lastName,
                        onTap: () => getItService.navigatorService.onChangeProfile(
                          type: TypeChangeProfile.lastName,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String title;
  final String value;
  final Function() onTap;

  const _Button({
    super.key,
    required this.value,
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(13),
        child: Row(
          children: [
            Text(title),
            const SizedBox(width: 10),
            Expanded(
              child: Text(
                value == '' ? 'Настроить' : value,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: value == '' ? Colors.grey : Colors.black54,
                ),
              ),
            ),
            const SizedBox(width: 10),
            const Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }
}
