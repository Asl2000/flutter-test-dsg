import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

enum TypeChangeProfile {
  firstName,
  lastName,
}

class ChangeProfileScreen extends StatefulWidget {
  final TypeChangeProfile type;

  const ChangeProfileScreen({
    super.key,
    required this.type,
  });

  @override
  State<ChangeProfileScreen> createState() => _ChangeProfileScreenState();
}

class _ChangeProfileScreenState extends State<ChangeProfileScreen> {
  late Store<AppState> store;
  final controller = TextEditingController();
  final profileInteractor = getItService.profileInteractor;

  @override
  void initState() {
    super.initState();
    store = StoreProvider.of<AppState>(context, listen: false);
    controller.text = store.state.profileState.profile!.firstName;
    if (widget.type == TypeChangeProfile.lastName) {
      controller.text = store.state.profileState.profile!.lastName;
    }
  }

  String get title => widget.type == TypeChangeProfile.firstName ? 'Ваше Имя' : 'Ваша Фамилия';

  void onBack() {
    final profile = store.state.profileState.profile!;
    if (widget.type == TypeChangeProfile.firstName) {
      if (profile.firstName != controller.text && controller.text != '') {
        profile.firstName = controller.text;
      }
    } else {
      if (profile.lastName != controller.text && controller.text != '') {
        profile.lastName = controller.text;
      }
    }
    profileInteractor.updateName(profile: profile);
    store.dispatch(ShowProfileAction(profile: profile));
    getItService.navigatorService.onBack();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        leadingWidth: 100,
        leading: GestureDetector(
          onTap: onBack,
          child: Container(
            color: Colors.transparent,
            child: const Row(
              children: [
                SizedBox(width: 10),
                Icon(
                  Icons.arrow_back_ios,
                  color: AppColors.secondaryColor,
                ),
                Text(
                  'Аккаунт',
                  style: TextStyle(
                    color: AppColors.secondaryColor,
                  ),
                ),
              ],
            ),
          ),
        ),
        centerTitle: true,
        title: Text(title),
      ),
      body: Container(
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1,
              color: Colors.grey,
            ),
          ),
        ),
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          children: [
            const SizedBox(height: 24),
            TextFormField(
              controller: controller,
              decoration: InputDecoration(hintText: title),
              validator: (value) {
                if (value!.isEmpty) return 'Заполните поле';
                return null;
              },
            ),
          ],
        ),
      ),
    );
  }
}
