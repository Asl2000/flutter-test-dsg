// ignore_for_file: unused_element, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:flutter_dsg/ui/resurses/icons.dart';
import 'package:flutter_dsg/ui/screens/photo/photo_screen.dart';
import 'package:flutter_dsg/ui/screens/profile/widgets/pick_photo_bottom_sheet.dart';
import 'package:flutter_dsg/ui/widgets/cached_image.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Avatar extends StatelessWidget {
  final String? avatar;

  const Avatar({
    super.key,
    required this.avatar,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: GestureDetector(
              onTap: () => showDialog(
                context: context,
                builder: (BuildContext context) {
                  return PhotoScreen(url: avatar ?? '');
                },
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: CashedImage(
                  width: 85,
                  height: 85,
                  url: avatar ?? '',
                  loadWidget: Container(
                    width: 85,
                    height: 85,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.shade300,
                    ),
                    child: const CircularProgressIndicator(
                      color: AppColors.secondaryColor,
                    ),
                  ),
                  errorWidget: const _Placeholder(),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () => showModalBottomSheet(
              context: context,
              backgroundColor: Colors.transparent,
              builder: (_) => const PickPhotoBottomSheet(),
            ),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).colorScheme.background,
              ),
              child: Icon(
                Icons.more_horiz,
                size: 30,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _Placeholder extends StatelessWidget {
  const _Placeholder({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 85,
      height: 85,
      padding: const EdgeInsets.only(
        top: 13,
        bottom: 7,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey.shade300,
      ),
      child: SvgPicture.asset(
        AppIcons.profile,
        color: Theme.of(context).colorScheme.secondary,
        allowDrawingOutsideViewBox: true,
      ),
    );
  }
}
