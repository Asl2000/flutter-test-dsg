// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:image_picker/image_picker.dart';

class PickPhotoBottomSheet extends StatefulWidget {
  const PickPhotoBottomSheet({super.key});

  @override
  State<PickPhotoBottomSheet> createState() => _PickPhotoBottomSheetState();
}

class _PickPhotoBottomSheetState extends State<PickPhotoBottomSheet> {
  final _picker = ImagePicker();
  final profileInteractor = getItService.profileInteractor;

  Future<void> setImage(ImageSource source) async {
    final pickedFile = await _picker.pickImage(source: source);
    getItService.navigatorService.onBack();
    if (pickedFile != null) {
      profileInteractor.setPhoto(file: pickedFile);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.background,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12),
                child: Text(
                  'Выберите фото',
                  style: Theme.of(context).textTheme.labelSmall,
                ),
              ),
              const Divider(
                thickness: 1,
                height: 1,
                color: Colors.grey,
              ),
              GestureDetector(
                onTap: () => setImage(ImageSource.camera),
                child: Container(
                  color: Colors.transparent,
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 18),
                  child: Text(
                    'Камера',
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                          color: AppColors.secondaryColor,
                          fontWeight: FontWeight.w400,
                        ),
                  ),
                ),
              ),
              const Divider(
                thickness: 1,
                height: 1,
                color: Colors.grey,
              ),
              GestureDetector(
                onTap: () => setImage(ImageSource.gallery),
                child: Container(
                  color: Colors.transparent,
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 18),
                  child: Text(
                    'Галерея Фото',
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                          color: AppColors.secondaryColor,
                          fontWeight: FontWeight.w400,
                        ),
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 12),
        GestureDetector(
          onTap: getItService.navigatorService.onBack,
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 16),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.circular(12),
            ),
            alignment: Alignment.center,
            child: Text(
              'Закрыть',
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                    color: AppColors.secondaryColor,
                  ),
            ),
          ),
        ),
        const SizedBox(height: 12),
      ],
    );
  }
}
