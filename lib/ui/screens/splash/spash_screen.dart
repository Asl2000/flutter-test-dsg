// ignore_for_file: constant_identifier_names

import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/images.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  static const TIMEOUT_SECONDS = 2;
  final navigatorService = getItService.navigatorService;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() {
    const duration = Duration(seconds: TIMEOUT_SECONDS);
    return Timer(duration, navigate);
  }

  void navigate() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) {
      navigatorService.onRegistration();
    } else if ((user.displayName ?? '') == '') {
      navigatorService.onDataRegistration();
    } else {
      navigatorService.onNavigator();
    }
  }

  double get size => MediaQuery.of(context).size.width * 0.7;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          AppImage.logo,
          width: size,
          height: size,
        ),
      ),
    );
  }
}
