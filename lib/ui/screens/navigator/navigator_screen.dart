// ignore_for_file: deprecated_member_use, unused_element

import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/resurses/icons.dart';
import 'package:flutter_dsg/ui/screens/my_projects/my_projects_screen.dart';
import 'package:flutter_dsg/ui/screens/profile/profile_screen.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NavigatorScreen extends StatefulWidget {
  const NavigatorScreen({super.key});

  @override
  State<NavigatorScreen> createState() => _NavigatorScreenState();
}

class _NavigatorScreenState extends State<NavigatorScreen> {
  late PageController _pageController;
  late int currentIndex;

  @override
  void initState() {
    super.initState();
    currentIndex = 0;
    _pageController = PageController(initialPage: currentIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  final _screens = [
    const MyProjectsScreen(),
    const ProfileScreen(),
  ];

  void _switch(int index) {
    _pageController.jumpToPage(index);
    setState(() => currentIndex = _pageController.page!.toInt());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: _screens,
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1.5,
              color: Colors.grey.shade300,
            ),
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: _ItemMenuIcon(
                onTap: () => _switch(0),
                icon: SvgPicture.asset(
                  currentIndex == 0 ? AppIcons.activeProjects : AppIcons.projects,
                  allowDrawingOutsideViewBox: true,
                ),
                title: 'Мои проекты',
                active: currentIndex == 0,
              ),
            ),
            Expanded(
              child: _ItemMenuIcon(
                onTap: () => _switch(1),
                active: currentIndex == 1,
                icon: SvgPicture.asset(
                  AppIcons.profile,
                  color: currentIndex == 1 ? Theme.of(context).colorScheme.secondary : null,
                  allowDrawingOutsideViewBox: true,
                ),
                title: 'Мой аккаунт',
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ItemMenuIcon extends StatelessWidget {
  final Widget icon;
  final bool active;
  final String title;
  final Function() onTap;

  const _ItemMenuIcon({
    super.key,
    required this.icon,
    required this.title,
    required this.active,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.grey.shade100,
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: icon,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.labelSmall!.copyWith(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: active ? Theme.of(context).colorScheme.secondary : null,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
