import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/enums/status.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';

class IconStatus extends StatelessWidget {
  final Status status;

  const IconStatus({
    super.key,
    required this.status,
  });

  Color get color {
    if (status == Status.alive) return AppColors.successColor;
    if (status == Status.dead) return AppColors.dangerColor;
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
