import 'package:flutter/material.dart';
import 'package:flutter_dsg/data/models/project/project.dart';
import 'package:flutter_dsg/ui/screens/my_projects/widgets/icon_status.dart';
import 'package:flutter_dsg/ui/screens/photo/photo_screen.dart';
import 'package:flutter_dsg/ui/widgets/cached_image.dart';

class ProjectCard extends StatelessWidget {
  final Project project;

  const ProjectCard({super.key, required this.project});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showDialog(
        context: context,
        builder: (BuildContext context) {
          return PhotoScreen(url: project.image);
        },
      ),
      child: Stack(
        children: [
          CashedImage(
            radius: 10,
            url: project.image,
            width: 300,
            height: 300,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: IconStatus(status: project.status),
              ),
              const Spacer(),
              Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(.7),
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(15),
                  ),
                ),
                padding: const EdgeInsets.all(7),
                child: Text(
                  project.name,
                  style: Theme.of(context).textTheme.labelSmall!.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
