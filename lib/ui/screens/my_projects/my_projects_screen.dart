import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/screens/my_projects/widgets/project_card.dart';
import 'package:flutter_dsg/ui/state_manager/project/actions.dart';
import 'package:flutter_dsg/ui/state_manager/project/states.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class MyProjectsScreen extends StatefulWidget {
  const MyProjectsScreen({super.key});

  @override
  State<MyProjectsScreen> createState() => _MyProjectsScreenState();
}

class _MyProjectsScreenState extends State<MyProjectsScreen> with SingleTickerProviderStateMixin {
  final scrollController = ScrollController();
  late Store<AppState> store;

  @override
  void initState() {
    super.initState();
    store = StoreProvider.of<AppState>(context, listen: false);
    if (store.state.projectListState.projects.isEmpty) store.dispatch(LoadProjectListAction());
    scrollController.addListener(() {
      if (scrollController.position.pixels + 600 > scrollController.position.maxScrollExtent) {
        store.dispatch(AddNextProjectListAction(page: store.state.projectListState.page));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        centerTitle: true,
        title: GestureDetector(
          onTap: () => scrollController.animateTo(
            0,
            duration: const Duration(milliseconds: 300),
            curve: Curves.ease,
          ),
          child: const Text('Проекты'),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1,
              color: Colors.grey,
            ),
          ),
        ),
        child: StoreConnector<AppState, ProjectListState>(
          converter: (store) => store.state.projectListState,
          builder: (BuildContext context, ProjectListState state) {
            if (state.isLoading) {
              return const Center(
                child: CircularProgressIndicator(color: Colors.white),
              );
            }
            if (state.isError) {
              return Center(
                child: Text(
                  state.errorMessage,
                  textAlign: TextAlign.center,
                ),
              );
            }

            return GridView.builder(
              controller: scrollController,
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 20,
              ),
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio: 1,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
              itemCount: state.projects.length,
              itemBuilder: (BuildContext ctx, index) {
                return ProjectCard(project: state.projects[index]);
              },
            );
          },
        ),
      ),
    );
  }
}
