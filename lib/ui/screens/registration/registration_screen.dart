import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dsg/data/links/links.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/masks/mask_phone_number.dart';
import 'package:flutter_dsg/ui/screens/registration/widgets/reg_spet_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({super.key});

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final phoneController = TextEditingController();
  bool canSend = false;
  final authInteractor = getItService.authInteractor;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    phoneController.addListener(() {
      if (phoneController.text.length == 18) {
        setState(() => canSend = true);
      } else {
        setState(() => canSend = false);
      }
    });
  }

  void openPrivacyPolicy() async {
    final Uri url = Uri.parse(Links.policy);
    await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * .05),
              const RegStepWidget(currencyStep: 1),
              const SizedBox(height: 24),
              Text(
                'Регистрация',
                style: Theme.of(context).textTheme.displayLarge,
              ),
              const SizedBox(height: 24),
              Text(
                'Введите номер регистрации\nдля регистрации',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodySmall,
              ),
              const SizedBox(height: 38),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Номер телефона',
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                  const SizedBox(height: 2),
                  TextField(
                    controller: phoneController,
                    keyboardType: TextInputType.number,
                    inputFormatters: [maskPhoneNumber],
                    decoration: const InputDecoration(
                      hintText: '+7 (___) ___-__-__',
                    ),
                  ),
                ],
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: canSend
                        ? () => authInteractor.verifyPhoneNumber(
                              phone: phoneController.text,
                              context: context,
                            )
                        : null,
                    child: const Text('Отправить смс-код'),
                  ),
                ),
              ),
              const SizedBox(height: 8),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: Theme.of(context).textTheme.labelSmall!.copyWith(
                        fontWeight: FontWeight.w400,
                        color: Colors.grey,
                      ),
                  children: <TextSpan>[
                    const TextSpan(
                      text: 'Нажимая на данную кнопку, вы даете\nсогласие на обработку ',
                    ),
                    TextSpan(
                      text: 'персональных данных',
                      style: TextStyle(color: Theme.of(context).primaryColor),
                      recognizer: TapGestureRecognizer()..onTap = openPrivacyPolicy,
                    ),
                  ],
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
