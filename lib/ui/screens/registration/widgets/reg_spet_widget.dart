import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';

class RegStepWidget extends StatelessWidget {
  final int countStep;
  final int currencyStep;

  const RegStepWidget({
    super.key,
    this.countStep = 3,
    required this.currencyStep,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: (60 * countStep).toDouble(),
          height: 1,
          color: AppColors.greyColor,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            for (int i = 0; i < countStep; i++) ...[
              Container(
                width: 45,
                height: 45,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: i < (currencyStep - 1)
                      ? Border.all(
                          color: AppColors.successColor,
                          width: 1,
                        )
                      : null,
                  shape: BoxShape.circle,
                  color: i < (currencyStep - 1)
                      ? Theme.of(context).scaffoldBackgroundColor
                      : (currencyStep - 1) == i
                          ? Theme.of(context).primaryColor
                          : AppColors.greyColor,
                ),
                child: i < (currencyStep - 1)
                    ? null
                    : Text(
                        (i + 1).toString(),
                      ),
              ),
              if (i < (currencyStep - 1))
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Icon(Icons.check, color: Colors.grey),
                )
              else if (i != countStep - 1)
                const SizedBox(width: 35)
            ],
          ],
        ),
      ],
    );
  }
}
