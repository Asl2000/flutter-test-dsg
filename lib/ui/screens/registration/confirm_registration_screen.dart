import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/screens/registration/widgets/reg_spet_widget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ConfirmRegistrationScreen extends StatefulWidget {
  final String phone;
  final String verificationId;

  const ConfirmRegistrationScreen({
    super.key,
    required this.phone,
    required this.verificationId,
  });

  @override
  State<ConfirmRegistrationScreen> createState() => _ConfirmRegistrationScreenState();
}

class _ConfirmRegistrationScreenState extends State<ConfirmRegistrationScreen> {
  final codeController = TextEditingController();
  final authInteractor = getItService.authInteractor;
  late int _countdownTime = 60;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    startCountdownTimer();
  }

  void startCountdownTimer() {
    _timer = Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) => setState(() {
        if (_countdownTime < 1) {
          _timer.cancel();
        } else {
          _countdownTime = _countdownTime - 1;
        }
      }),
    );
  }

  void checkCode(String code) {
    authInteractor.confirmCode(
      code: code,
      verificationId: widget.verificationId,
      context: context,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * .03),
              Row(
                children: [
                  IconButton(
                    onPressed: getItService.navigatorService.onBack,
                    icon: const Icon(Icons.arrow_back_ios),
                  ),
                ],
              ),
              const RegStepWidget(currencyStep: 2),
              const SizedBox(height: 24),
              Text(
                'Подтверждение',
                style: Theme.of(context).textTheme.displayLarge,
              ),
              const SizedBox(height: 24),
              Text(
                'Введите код, который мы отправили\nв SMS на ${widget.phone}',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodySmall,
              ),
              const SizedBox(height: 38),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: PinCodeTextField(
                  autoFocus: true,
                  textStyle: Theme.of(context).textTheme.displayMedium,
                  backgroundColor: Colors.white,
                  appContext: context,
                  length: 6,
                  animationType: AnimationType.fade,
                  pinTheme: PinTheme(
                    borderRadius: BorderRadius.circular(10),
                    fieldHeight: 40,
                    fieldWidth: 40,
                    activeFillColor: Theme.of(context).scaffoldBackgroundColor,
                    inactiveFillColor: Theme.of(context).scaffoldBackgroundColor,
                    selectedFillColor: Theme.of(context).scaffoldBackgroundColor,
                    activeColor: Colors.grey,
                    selectedColor: Colors.grey,
                    inactiveColor: Colors.grey,
                    errorBorderColor: Colors.red,
                  ),
                  controller: codeController,
                  animationDuration: const Duration(milliseconds: 300),
                  enableActiveFill: true,
                  keyboardType: TextInputType.number,
                  onCompleted: checkCode,
                  onChanged: (value) {},
                ),
              ),
              const SizedBox(height: 44),
              if (_countdownTime == 0)
                TextButton(
                  onPressed: () {},
                  child: const Text('Отправить код еще раз'),
                )
              else
                Text(
                  '$_countdownTime сек до повтора отправки кода',
                  style: Theme.of(context).textTheme.bodySmall,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
