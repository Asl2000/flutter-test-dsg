import 'package:flutter/material.dart';
import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:flutter_dsg/domain/di/get_it_service.dart';
import 'package:flutter_dsg/ui/resurses/buttons_style.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:flutter_dsg/ui/screens/registration/widgets/reg_spet_widget.dart';

class DataRegistrationScreen extends StatefulWidget {
  const DataRegistrationScreen({super.key});

  @override
  State<DataRegistrationScreen> createState() => _DataRegistrationScreenState();
}

class _DataRegistrationScreenState extends State<DataRegistrationScreen> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final authInteractor = getItService.authInteractor;
  final _formKey = GlobalKey<FormState>();

  void save() {
    if (!_formKey.currentState!.validate()) return;
    _formKey.currentState!.save();
    final profile = Profile(
      firstName: firstNameController.text,
      lastName: lastNameController.text,
    );
    authInteractor.registerData(
      profile: profile,
      context: context,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            const RegStepWidget(currencyStep: 3),
            const SizedBox(height: 24),
            Text(
              'Данные',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayLarge,
            ),
            const SizedBox(height: 24),
            Text(
              'Заполните данные о себе',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodySmall,
            ),
            const SizedBox(height: 38),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Имя',
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                  const SizedBox(height: 2),
                  TextFormField(
                    controller: firstNameController,
                    decoration: const InputDecoration(hintText: 'Ваше Имя'),
                    validator: (value) {
                      if (value!.isEmpty) return 'Заполните поле';
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Фамилия',
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                  const SizedBox(height: 2),
                  TextFormField(
                    controller: lastNameController,
                    decoration: const InputDecoration(hintText: 'Ваша Фамилия'),
                    validator: (value) {
                      if (value!.isEmpty) return 'Заполните поле';
                      return null;
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: save,
                      child: const Text('Зарегестрироваться'),
                    ),
                  ),
                  const SizedBox(height: 16),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: authInteractor.logOut,
                      style: AppButtonStyle.solidButton(color: AppColors.primaryColor),
                      child: const Text('Выйти'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
