class AppIcons {
  static const profile = 'assets/icons/profile.svg';
  static const projects = 'assets/icons/projects.svg';
  static const activeProjects = 'assets/icons/active_projects.svg';
}
