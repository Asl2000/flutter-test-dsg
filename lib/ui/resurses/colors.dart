import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color.fromRGBO(255, 184, 0, 1);
  static const successColor = Color.fromRGBO(57, 163, 20, 1);
  static const greyColor = Color.fromRGBO(217, 217, 217, 1);
  static const secondaryColor = Color.fromRGBO(0, 152, 238, 1);
  static const dangerColor = Color(0XFFEA5644);
  static const backgroundColor = Color.fromRGBO(246, 246, 246, 1);
}
