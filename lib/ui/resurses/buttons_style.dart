import 'package:flutter/material.dart';

class AppButtonStyle {
  static ButtonStyle solidButton({required Color color}) {
    return ElevatedButton.styleFrom(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(
          width: 2,
          color: color,
        ),
      ),
    );
  }
}
