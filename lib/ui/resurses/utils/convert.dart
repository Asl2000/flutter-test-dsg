import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String convertTimer({required int time}) {
  int minutes = Duration(seconds: time).inMinutes;
  int seconds = time - (minutes * 60);
  String sec = '';
  if (seconds < 10) {
    sec = '0$seconds';
  } else {
    sec = seconds.toString();
  }

  String min = '';
  if (seconds < 10) {
    min = '0$minutes';
  } else {
    min = minutes.toString();
  }
  return '$min:$sec';
}

String? convertFloat(double? number) {
  if (number == null) return null;
  final d = number.toString().split('.');
  if (d.last == '0') return d.first;
  return number.toString();
}

String convertDate({
  required DateTime? date,
  bool withTime = false,
  Locale? monthLocale,
}) {
  if (date != null) {
    if (withTime) {
      if (monthLocale == null) return DateFormat('dd.MM.yyyy HH:mm').format(date);
      return DateFormat('dd MMMM yyyy HH:mm', monthLocale.languageCode).format(date);
    } else {
      if (monthLocale == null) return DateFormat('dd.MM.yyyy').format(date);
      return DateFormat('dd MMMM yyyy', monthLocale.languageCode).format(date);
    }
  } else {
    return '';
  }
}
