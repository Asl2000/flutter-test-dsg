import 'package:flutter/material.dart';

void showLoading({required BuildContext context}) {
  showDialog(
    context: context,
    builder: (context) {
      return Container(
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
              ),
              child: const CircularProgressIndicator(),
            ),
          ],
        ),
      );
    },
  );
}

void closeLoading({required BuildContext context}) => Navigator.of(context).pop();
