import 'package:flutter_dsg/data/models/profile/profile.dart';

abstract class ProfileAction {}

class LoadProfileAction extends ProfileAction {}

class ShowProfileAction extends ProfileAction {
  final Profile profile;

  ShowProfileAction({required this.profile});
}

class ErrorProfileAction extends ProfileAction {
  final String message;

  ErrorProfileAction({required this.message});
}
