import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'states.freezed.dart';

@freezed
class ProfileState with _$ProfileState {
  factory ProfileState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    Profile? profile,
  ]) = _ProfileState;
}
