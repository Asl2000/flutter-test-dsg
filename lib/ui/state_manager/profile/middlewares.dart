import 'package:flutter_dsg/domain/repositories/profile_repository.dart';
import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:redux/redux.dart';

class ProfileMiddleware implements MiddlewareClass<AppState> {
  final ProfileRepository profileRepository;

  ProfileMiddleware({required this.profileRepository});

  @override
  call(store, action, next) {
    // Self user
    if (action is LoadProfileAction) {
      Future(() async {
        final profile = await profileRepository.getProfile();
        if (profile != null) {
          store.dispatch(ShowProfileAction(profile: profile));
        } else {
          store.dispatch(ErrorProfileAction(message: 'Пользователь не найден'));
        }
      }).onError((err, stackTrace) => store.dispatch(ErrorProfileAction(message: '$err')));
    }

    next(action);
  }
}
