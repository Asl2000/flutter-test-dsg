import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/profile/states.dart';
import 'package:redux/redux.dart';

final profileReducer = combineReducers<ProfileState>([
  TypedReducer<ProfileState, LoadProfileAction>(_loadUserSelf).call,
  TypedReducer<ProfileState, ErrorProfileAction>(_errorUserSelf).call,
  TypedReducer<ProfileState, ShowProfileAction>(_showUserSelf).call,
]);

ProfileState _loadUserSelf(
  ProfileState state,
  LoadProfileAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

ProfileState _showUserSelf(
  ProfileState state,
  ShowProfileAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      profile: action.profile,
    );

ProfileState _errorUserSelf(
  ProfileState state,
  ErrorProfileAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      errorMessage: action.message,
    );
