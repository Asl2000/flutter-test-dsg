import 'package:flutter_dsg/data/models/project/project.dart';

abstract class ProjectListAction {}

class LoadProjectListAction extends ProjectListAction {}

class ShowProjectListAction extends ProjectListAction {
  final List<Project> projects;
  final int page;

  ShowProjectListAction({
    required this.projects,
    required this.page,
  });
}

class ErrorProjectListAction extends ProjectListAction {
  final String message;

  ErrorProjectListAction({required this.message});
}

class AddNextProjectListAction extends ProjectListAction {
  final int page;

  AddNextProjectListAction({required this.page});
}

class StopProjectListAction extends ProjectListAction {}
