import 'package:flutter_dsg/data/models/project/project.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'states.freezed.dart';

@freezed
class ProjectListState with _$ProjectListState {
  factory ProjectListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default(0) int page,
    @Default('') String errorMessage,
    @Default([]) List<Project> projects,
    @Default(true) bool isNextPageAvailable,
    @Default(false) bool isLoadingNextPage,
  ]) = _ProjectListState;
}
