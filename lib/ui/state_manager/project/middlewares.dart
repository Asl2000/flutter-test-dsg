import 'package:flutter_dsg/domain/repositories/project_repository.dart';
import 'package:flutter_dsg/ui/state_manager/project/actions.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:redux/redux.dart';

class ProjectMiddleware implements MiddlewareClass<AppState> {
  final ProjectRepository projectRepository;

  ProjectMiddleware({required this.projectRepository});

  @override
  call(store, action, next) {
    // List Projects
    if (action is LoadProjectListAction) {
      Future(() async {
        final projects = await projectRepository.getListProject(page: 0);
        if (projects != null) {
          store.dispatch(ShowProjectListAction(
            projects: projects,
            page: 0,
          ));
        } else {
          store.dispatch(ErrorProjectListAction(message: 'Error load Projects'));
        }
      });
    }

    if (action is AddNextProjectListAction) {
      Future(() async {
        final page = store.state.projectListState.page + 1;
        final projectsOld = store.state.projectListState.projects.toList();
        final projects = await projectRepository.getListProject(page: page);
        if ((projects ?? []).isEmpty) {
          store.dispatch(StopProjectListAction());
        } else {
          projectsOld.addAll(projects!);
          store.dispatch(ShowProjectListAction(
            projects: projectsOld,
            page: page,
          ));
        }
      });
    }

    next(action);
  }
}
