import 'package:flutter_dsg/ui/state_manager/project/actions.dart';
import 'package:flutter_dsg/ui/state_manager/project/states.dart';
import 'package:redux/redux.dart';

final projectListReducer = combineReducers<ProjectListState>([
  TypedReducer<ProjectListState, LoadProjectListAction>(_loadProjectList).call,
  TypedReducer<ProjectListState, ShowProjectListAction>(_showProjectList).call,
  TypedReducer<ProjectListState, ErrorProjectListAction>(_errorProjectList).call,
  TypedReducer<ProjectListState, StopProjectListAction>(_stopProjectList).call,
  TypedReducer<ProjectListState, AddNextProjectListAction>(_addNextProjectList).call,
]);

//List Projects

ProjectListState _loadProjectList(
  ProjectListState state,
  LoadProjectListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
      isLoadingNextPage: true,
      isNextPageAvailable: true,
    );

ProjectListState _stopProjectList(
  ProjectListState state,
  StopProjectListAction action,
) =>
    state.copyWith(
      isNextPageAvailable: false,
      isLoadingNextPage: false,
    );

ProjectListState _addNextProjectList(
  ProjectListState state,
  AddNextProjectListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isLoadingNextPage: true,
      isError: false,
    );

ProjectListState _showProjectList(
  ProjectListState state,
  ShowProjectListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      projects: action.projects,
      page: action.page,
      isLoadingNextPage: false,
    );

ProjectListState _errorProjectList(
  ProjectListState state,
  ErrorProjectListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      errorMessage: action.message,
      isLoadingNextPage: false,
    );
