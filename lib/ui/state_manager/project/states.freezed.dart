// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'states.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProjectListState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isError => throw _privateConstructorUsedError;
  int get page => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;
  List<Project> get projects => throw _privateConstructorUsedError;
  bool get isNextPageAvailable => throw _privateConstructorUsedError;
  bool get isLoadingNextPage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProjectListStateCopyWith<ProjectListState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProjectListStateCopyWith<$Res> {
  factory $ProjectListStateCopyWith(
          ProjectListState value, $Res Function(ProjectListState) then) =
      _$ProjectListStateCopyWithImpl<$Res, ProjectListState>;
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      int page,
      String errorMessage,
      List<Project> projects,
      bool isNextPageAvailable,
      bool isLoadingNextPage});
}

/// @nodoc
class _$ProjectListStateCopyWithImpl<$Res, $Val extends ProjectListState>
    implements $ProjectListStateCopyWith<$Res> {
  _$ProjectListStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? page = null,
    Object? errorMessage = null,
    Object? projects = null,
    Object? isNextPageAvailable = null,
    Object? isLoadingNextPage = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      projects: null == projects
          ? _value.projects
          : projects // ignore: cast_nullable_to_non_nullable
              as List<Project>,
      isNextPageAvailable: null == isNextPageAvailable
          ? _value.isNextPageAvailable
          : isNextPageAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingNextPage: null == isLoadingNextPage
          ? _value.isLoadingNextPage
          : isLoadingNextPage // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProjectListStateImplCopyWith<$Res>
    implements $ProjectListStateCopyWith<$Res> {
  factory _$$ProjectListStateImplCopyWith(_$ProjectListStateImpl value,
          $Res Function(_$ProjectListStateImpl) then) =
      __$$ProjectListStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      int page,
      String errorMessage,
      List<Project> projects,
      bool isNextPageAvailable,
      bool isLoadingNextPage});
}

/// @nodoc
class __$$ProjectListStateImplCopyWithImpl<$Res>
    extends _$ProjectListStateCopyWithImpl<$Res, _$ProjectListStateImpl>
    implements _$$ProjectListStateImplCopyWith<$Res> {
  __$$ProjectListStateImplCopyWithImpl(_$ProjectListStateImpl _value,
      $Res Function(_$ProjectListStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? page = null,
    Object? errorMessage = null,
    Object? projects = null,
    Object? isNextPageAvailable = null,
    Object? isLoadingNextPage = null,
  }) {
    return _then(_$ProjectListStateImpl(
      null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      null == projects
          ? _value._projects
          : projects // ignore: cast_nullable_to_non_nullable
              as List<Project>,
      null == isNextPageAvailable
          ? _value.isNextPageAvailable
          : isNextPageAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      null == isLoadingNextPage
          ? _value.isLoadingNextPage
          : isLoadingNextPage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ProjectListStateImpl implements _ProjectListState {
  _$ProjectListStateImpl(
      [this.isLoading = false,
      this.isError = false,
      this.page = 0,
      this.errorMessage = '',
      final List<Project> projects = const [],
      this.isNextPageAvailable = true,
      this.isLoadingNextPage = false])
      : _projects = projects;

  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final bool isError;
  @override
  @JsonKey()
  final int page;
  @override
  @JsonKey()
  final String errorMessage;
  final List<Project> _projects;
  @override
  @JsonKey()
  List<Project> get projects {
    if (_projects is EqualUnmodifiableListView) return _projects;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_projects);
  }

  @override
  @JsonKey()
  final bool isNextPageAvailable;
  @override
  @JsonKey()
  final bool isLoadingNextPage;

  @override
  String toString() {
    return 'ProjectListState(isLoading: $isLoading, isError: $isError, page: $page, errorMessage: $errorMessage, projects: $projects, isNextPageAvailable: $isNextPageAvailable, isLoadingNextPage: $isLoadingNextPage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProjectListStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isError, isError) || other.isError == isError) &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            const DeepCollectionEquality().equals(other._projects, _projects) &&
            (identical(other.isNextPageAvailable, isNextPageAvailable) ||
                other.isNextPageAvailable == isNextPageAvailable) &&
            (identical(other.isLoadingNextPage, isLoadingNextPage) ||
                other.isLoadingNextPage == isLoadingNextPage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      isLoading,
      isError,
      page,
      errorMessage,
      const DeepCollectionEquality().hash(_projects),
      isNextPageAvailable,
      isLoadingNextPage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProjectListStateImplCopyWith<_$ProjectListStateImpl> get copyWith =>
      __$$ProjectListStateImplCopyWithImpl<_$ProjectListStateImpl>(
          this, _$identity);
}

abstract class _ProjectListState implements ProjectListState {
  factory _ProjectListState(
      [final bool isLoading,
      final bool isError,
      final int page,
      final String errorMessage,
      final List<Project> projects,
      final bool isNextPageAvailable,
      final bool isLoadingNextPage]) = _$ProjectListStateImpl;

  @override
  bool get isLoading;
  @override
  bool get isError;
  @override
  int get page;
  @override
  String get errorMessage;
  @override
  List<Project> get projects;
  @override
  bool get isNextPageAvailable;
  @override
  bool get isLoadingNextPage;
  @override
  @JsonKey(ignore: true)
  _$$ProjectListStateImplCopyWith<_$ProjectListStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
