import 'package:flutter_dsg/ui/state_manager/profile/states.dart';
import 'package:flutter_dsg/ui/state_manager/project/states.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'store.freezed.dart';

@freezed
class AppState with _$AppState {
  const factory AppState({
    required ProfileState profileState,
    required ProjectListState projectListState,
  }) = _AppState;
}
