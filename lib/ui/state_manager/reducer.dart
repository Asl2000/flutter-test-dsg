import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/profile/reducers.dart';
import 'package:flutter_dsg/ui/state_manager/project/actions.dart';
import 'package:flutter_dsg/ui/state_manager/project/reducers.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';

AppState appReducer(AppState state, dynamic action) {
  if (action is ProfileAction) {
    final nextState = profileReducer(state.profileState, action);
    return state.copyWith(profileState: nextState);
  } else if (action is ProjectListAction) {
    final nextState = projectListReducer(state.projectListState, action);
    return state.copyWith(projectListState: nextState);
  }
  return state;
}
