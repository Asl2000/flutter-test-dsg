import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/screens/navigator/navigator_screen.dart';
import 'package:flutter_dsg/ui/screens/profile/change_profile_screen.dart';
import 'package:flutter_dsg/ui/screens/registration/confirm_registration_screen.dart';
import 'package:flutter_dsg/ui/screens/registration/data_registration_screen.dart';
import 'package:flutter_dsg/ui/screens/registration/registration_screen.dart';
import 'package:flutter_dsg/ui/screens/splash/spash_screen.dart';

class AppRouter {
  static const String splash = '/splash';
  static const String registration = '/registration';
  static const String confirmRegistration = '/confirm_registration';
  static const String dataRegistration = '/data_registration';
  static const String navigator = '/navigator';
  static const String changeProfile = '/change_profile';

  static MaterialPageRoute onGenerateRouters(RouteSettings settings) {
    final args = settings.arguments as AppRouterArguments?;
    final routes = <String, WidgetBuilder>{
      splash: (_) => const SplashScreen(),
      registration: (_) => const RegistrationScreen(),
      dataRegistration: (_) => const DataRegistrationScreen(),
      navigator: (_) => const NavigatorScreen(),
      confirmRegistration: (_) => ConfirmRegistrationScreen(
            phone: args!.phone!,
            verificationId: args.verificationId!,
          ),
      changeProfile: (_) => ChangeProfileScreen(type: args!.typeChangeProfile!),
    };

    WidgetBuilder? builder = routes[settings.name!];
    return MaterialPageRoute(builder: (ctx) => builder!(ctx));
  }
}

class AppRouterArguments {
  final String? phone;
  final String? verificationId;
  final TypeChangeProfile? typeChangeProfile;

  AppRouterArguments({
    this.phone,
    this.verificationId,
    this.typeChangeProfile,
  });
}
