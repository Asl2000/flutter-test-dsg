import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dsg/domain/di/locator.dart';
import 'package:flutter_dsg/domain/l10n/l10n.dart';
import 'package:flutter_dsg/router.dart';
import 'package:flutter_dsg/ui/resurses/theme/light_theme.dart';
import 'package:flutter_dsg/ui/screens/splash/spash_screen.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  final locator = AppLocator();

  runApp(App(locator: locator));
}

class App extends StatelessWidget {
  final AppLocator locator;

  const App({
    super.key,
    required this.locator,
  });

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: locator.store,
      child: OverlaySupport.global(
        child: MaterialApp(
          locale: L10n.languages.first.locale,
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: L10n.languages.map((e) => e.locale).toList(),
          theme: lightThemeData,
          navigatorKey: locator.navigatorKey,
          title: 'Test DSG',
          home: const SplashScreen(),
          onGenerateRoute: AppRouter.onGenerateRouters,
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
