import 'package:flutter/material.dart';
import 'package:flutter_dsg/data/models/language/language.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension LocalizedBuildContext on BuildContext {
  AppLocalizations get l10n => AppLocalizations.of(this)!;
}

class L10n {
  static List<Language> languages = <Language>[
    Language.russian(),
    Language.english(),
  ];
}
