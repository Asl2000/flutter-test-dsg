import 'package:flutter_dsg/data/models/project/project.dart';

abstract class ProjectRepository {
  Future<List<Project>?> getListProject({int? page});
}
