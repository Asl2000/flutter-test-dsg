import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:image_picker/image_picker.dart';

abstract class ProfileRepository {
  Future<Profile?> getProfile();

  Future<String?> setPhotoProfile({required XFile file});

  Future<bool> updateName({
    required String firstName,
    required String lastName,
  });
}
