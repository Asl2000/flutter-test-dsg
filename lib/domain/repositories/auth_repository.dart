import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_dsg/data/models/profile/profile.dart';

abstract class AuthRepository {
  Future<User?> confirmCode({required PhoneAuthCredential credential});

  Future<bool> registerData({required Profile profile});
}
