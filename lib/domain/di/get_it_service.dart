import 'package:flutter_dsg/domain/di/injector/injector.dart';
import 'package:flutter_dsg/domain/interactors/auth_interactor.dart';
import 'package:flutter_dsg/domain/interactors/profile_interactor.dart';
import 'package:flutter_dsg/domain/services/navigator_service.dart';

class GetItService {
  NavigatorService get navigatorService => getIt.get<NavigatorService>();

  AuthInteractor get authInteractor => getIt.get<AuthInteractor>();

  ProfileInteractor get profileInteractor => getIt.get<ProfileInteractor>();

  const GetItService._();
}

const getItService = GetItService._();
