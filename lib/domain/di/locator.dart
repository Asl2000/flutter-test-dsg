import 'package:flutter/material.dart';
import 'package:flutter_dsg/data/api/dio_http_client.dart';
import 'package:flutter_dsg/data/repositories/api_project_repository.dart';
import 'package:flutter_dsg/data/repositories/firebase_auth_repository.dart';
import 'package:flutter_dsg/data/repositories/firebase_profile_repository.dart';
import 'package:flutter_dsg/domain/interactors/auth_interactor.dart';
import 'package:flutter_dsg/domain/interactors/profile_interactor.dart';
import 'package:flutter_dsg/domain/repositories/auth_repository.dart';
import 'package:flutter_dsg/domain/repositories/project_repository.dart';
import 'package:flutter_dsg/domain/repositories/profile_repository.dart';
import 'package:flutter_dsg/domain/services/logger.dart';
import 'package:flutter_dsg/domain/services/navigator_service.dart';
import 'package:flutter_dsg/ui/state_manager/profile/middlewares.dart';
import 'package:flutter_dsg/ui/state_manager/profile/states.dart';
import 'package:flutter_dsg/ui/state_manager/project/middlewares.dart';
import 'package:flutter_dsg/ui/state_manager/project/states.dart';
import 'package:flutter_dsg/ui/state_manager/reducer.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';

class AppLocator {
  final getIt = GetIt.instance;
  final _httpClient = DioHttpClient();
  final navigatorKey = GlobalKey<NavigatorState>();

  late NavigatorService navigatorService;

  late ProfileRepository profileRepository;
  late AuthRepository authRepository;
  late ProjectRepository projectRepository;

  late AuthInteractor authInteractor;
  late ProfileInteractor profileInteractor;

  late Store<AppState> store;

  AppLocator() {
    _init();
  }

  void _init() {
    initLogger();
    navigatorService = NavigatorService(navigatorKey: navigatorKey);

    profileRepository = FirebaseProfileRepository();
    authRepository = FirebaseAuthRepository();
    projectRepository = ApiProjectRepository(httpClient: _httpClient);

    store = Store(
      appReducer,
      initialState: AppState(
        profileState: ProfileState(),
        projectListState: ProjectListState(),
      ),
      middleware: [
        ProfileMiddleware(profileRepository: profileRepository).call,
        ProjectMiddleware(projectRepository: projectRepository).call,
      ],
    );

    authInteractor = AuthInteractor(
      authRepository: authRepository,
      navigatorService: navigatorService,
    );
    profileInteractor = ProfileInteractor(
      profileRepository: profileRepository,
      store: store,
    );

    _register();
  }

  void _register() {
    getIt.registerSingleton<NavigatorService>(navigatorService);
    getIt.registerSingleton<AuthInteractor>(authInteractor);
    getIt.registerSingleton<ProfileInteractor>(profileInteractor);
  }
}
