import 'package:flutter/material.dart';
import 'package:flutter_dsg/ui/resurses/colors.dart';
import 'package:flutter_dsg/ui/resurses/text_style.dart';
import 'package:overlay_support/overlay_support.dart';

class ExceptionInteractor {
  static void showError({required String message}) {
    showOverlayNotification(
      (BuildContext context) {
        return Material(
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 30,
            ),
            color: AppColors.dangerColor,
            child: Text(
              message,
              style: bodyMedium.copyWith(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
      position: NotificationPosition.bottom,
    );
  }
}
