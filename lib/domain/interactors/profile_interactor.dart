// ignore_for_file: use_build_context_synchronously

import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:flutter_dsg/domain/repositories/profile_repository.dart';
import 'package:flutter_dsg/ui/state_manager/profile/actions.dart';
import 'package:flutter_dsg/ui/state_manager/store.dart';
import 'package:image_picker/image_picker.dart';
import 'package:redux/redux.dart';

class ProfileInteractor {
  final ProfileRepository _profileRepository;
  final Store<AppState> _store;

  ProfileInteractor({
    required ProfileRepository profileRepository,
    required Store<AppState> store,
  })  : _profileRepository = profileRepository,
        _store = store;

  void setPhoto({required XFile file}) async {
    final url = await _profileRepository.setPhotoProfile(file: file);
    if (url != null) {
      final profile = _store.state.profileState.profile!;
      profile.avatar = url;
      _store.dispatch(ShowProfileAction(profile: profile));
    }
  }

  void updateName({required Profile profile}) {
    _profileRepository.updateName(
      lastName: profile.lastName,
      firstName: profile.firstName,
    );
  }
}
