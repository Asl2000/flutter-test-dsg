// ignore_for_file: use_build_context_synchronously

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:flutter_dsg/domain/interactors/exception_interactor.dart';
import 'package:flutter_dsg/domain/repositories/auth_repository.dart';
import 'package:flutter_dsg/domain/services/navigator_service.dart';
import 'package:flutter_dsg/ui/resurses/utils/loading.dart';

class AuthInteractor {
  final AuthRepository _authRepository;
  final NavigatorService _navigatorService;

  AuthInteractor({
    required AuthRepository authRepository,
    required NavigatorService navigatorService,
  })  : _authRepository = authRepository,
        _navigatorService = navigatorService;

  void verifyPhoneNumber({
    required String phone,
    required BuildContext context,
  }) async {
    FocusScope.of(context).unfocus();
    showLoading(context: context);
    try {
      await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phone,
        verificationCompleted: (_) {},
        verificationFailed: (FirebaseAuthException e) {
          closeLoading(context: context);
          if (e.code == 'invalid-phone-number') {
            ExceptionInteractor.showError(message: 'Указанный номер телефона недействителен.');
          } else {
            ExceptionInteractor.showError(message: e.code);
          }
        },
        codeSent: (String id, _) {
          closeLoading(context: context);
          _navigatorService.onConfirmRegistration(
            phone: phone,
            verificationId: id,
          );
        },
        codeAutoRetrievalTimeout: (_) {},
      );
    } catch (e) {
      closeLoading(context: context);
      ExceptionInteractor.showError(message: e.toString());
    }
  }

  void confirmCode({
    required String code,
    required String verificationId,
    required BuildContext context,
  }) async {
    FocusScope.of(context).unfocus();
    showLoading(context: context);
    final credential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: code,
    );
    final user = await _authRepository.confirmCode(credential: credential);
    closeLoading(context: context);
    if (user == null) return;
    if ((user.displayName ?? '') == '') {
      _navigatorService.onDataRegistration();
    } else {
      _navigatorService.onNavigator();
    }
  }

  void registerData({
    required Profile profile,
    required BuildContext context,
  }) async {
    showLoading(context: context);
    final isConfirm = await _authRepository.registerData(profile: profile);
    closeLoading(context: context);
    if (isConfirm) _navigatorService.onNavigator();
  }

  void logOut() async {
    FirebaseAuth.instance.signOut();
    _navigatorService.onRegistration();
  }
}
