import 'package:flutter/material.dart';
import 'package:flutter_dsg/router.dart';
import 'package:flutter_dsg/ui/screens/profile/change_profile_screen.dart';

class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorService({required this.navigatorKey});

  bool canPop() => navigatorKey.currentState!.canPop();

  void onBack() => navigatorKey.currentState!.pop();

  void onNavigator() {
    navigatorKey.currentState!.pushNamedAndRemoveUntil(
      AppRouter.navigator,
      (route) => false,
    );
  }

  void onRegistration() {
    navigatorKey.currentState!.pushNamedAndRemoveUntil(
      AppRouter.registration,
      (route) => false,
    );
  }

  void onDataRegistration() {
    navigatorKey.currentState!.pushNamedAndRemoveUntil(
      AppRouter.dataRegistration,
      (route) => false,
    );
  }

  void onChangeProfile({required TypeChangeProfile type}) {
    navigatorKey.currentState!.pushNamed(
      AppRouter.changeProfile,
      arguments: AppRouterArguments(typeChangeProfile: type),
    );
  }

  void onConfirmRegistration({
    required String phone,
    required String verificationId,
  }) {
    navigatorKey.currentState!.pushNamed(
      AppRouter.confirmRegistration,
      arguments: AppRouterArguments(
        phone: phone,
        verificationId: verificationId,
      ),
    );
  }
}
