import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_dsg/data/api/api_urls.dart';
import 'package:flutter_dsg/data/api/http_client.dart';
import 'package:flutter_dsg/data/models/project/project.dart';
import 'package:flutter_dsg/domain/repositories/project_repository.dart';

class ApiProjectRepository implements ProjectRepository {
  final HttpClient _httpClient;

  ApiProjectRepository({required HttpClient httpClient}) : _httpClient = httpClient;

  @override
  Future<List<Project>?> getListProject({int? page}) async {
    try {
      final Map<String, dynamic> queryParameters = {};
      if (page != null) queryParameters['page'] = page;
      final response = await _httpClient.dio.get(
        ApiUrl.character,
        queryParameters: queryParameters,
      );
      final result = response.data!['results'] as List;
      return result.map((e) => Project.fromJson(e)).toList();
    } on DioException catch (e) {
      log('DioException: getListProject: $e');
      return null;
    }
  }
}
