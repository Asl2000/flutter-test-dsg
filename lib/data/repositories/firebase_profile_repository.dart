import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:flutter_dsg/domain/interactors/exception_interactor.dart';
import 'package:flutter_dsg/domain/repositories/profile_repository.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class FirebaseProfileRepository implements ProfileRepository {
  @override
  Future<Profile?> getProfile() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) return null;
    final data = (user.displayName ?? '').split('///');
    return Profile(
      avatar: user.photoURL,
      firstName: data.first,
      lastName: data.length > 1 ? data.last : '',
    );
  }

  @override
  Future<String?> setPhotoProfile({required XFile file}) async {
    try {
      final bytes = await file.readAsBytes();
      final storage = firebase_storage.FirebaseStorage.instance.ref('avatars/${file.name}');
      await storage.putData(bytes);
      final url = await storage.getDownloadURL();
      await FirebaseAuth.instance.currentUser?.updatePhotoURL(url);
      return url;
    } catch (e) {
      ExceptionInteractor.showError(message: e.toString());
      return null;
    }
  }

  @override
  Future<bool> updateName({
    required String firstName,
    required String lastName,
  }) async {
    try {
      await FirebaseAuth.instance.currentUser?.updateDisplayName(
        '$firstName///$lastName',
      );
      return true;
    } catch (e) {
      ExceptionInteractor.showError(message: e.toString());
      return false;
    }
  }
}
