import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_dsg/data/models/profile/profile.dart';
import 'package:flutter_dsg/domain/interactors/exception_interactor.dart';
import 'package:flutter_dsg/domain/repositories/auth_repository.dart';

class FirebaseAuthRepository implements AuthRepository {
  @override
  Future<User?> confirmCode({required PhoneAuthCredential credential}) async {
    try {
      final answer = await FirebaseAuth.instance.signInWithCredential(credential);
      return answer.user;
    } catch (e) {
      ExceptionInteractor.showError(message: e.toString());
      return null;
    }
  }

  @override
  Future<bool> registerData({required Profile profile}) async {
    try {
      await FirebaseAuth.instance.currentUser?.updateDisplayName(
        '${profile.firstName}///${profile.lastName}',
      );
      return true;
    } catch (e) {
      ExceptionInteractor.showError(message: e.toString());
      return false;
    }
  }
}
