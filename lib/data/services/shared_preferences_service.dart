// ignore_for_file: constant_identifier_names

import 'package:shared_preferences/shared_preferences.dart';

class _Keys {
  static const TOKEN = 'TOKEN';
}

class SharedPreferencesService {
  //TOKEN
  static Future<bool> setToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(_Keys.TOKEN, token);
  }

  static Future<String?> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString(_Keys.TOKEN);
    return token;
  }

  static Future<bool> removeToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(_Keys.TOKEN);
  }
}
