import 'package:flutter/cupertino.dart';

class Language {
  final Locale locale;
  final String name;

  Language({
    required this.locale,
    required this.name,
  });

  factory Language.russian() => Language(
    locale: const Locale('ru'),
    name: 'Русский',
  );

  factory Language.english() => Language(
    locale: const Locale('en'),
    name: 'English',
  );
}
