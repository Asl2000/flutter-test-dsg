import 'package:flutter_dsg/data/deserializers/status_deserializer.dart';
import 'package:flutter_dsg/domain/enums/status.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'project.g.dart';

@JsonSerializable()
class Project {
  final int id;
  final String name;
  final String _status;
  final String image;

  Project({
    required this.id,
    required this.name,
    required this.image,
    required String status,
  }) : _status = status;

  Status get status => StatusDeserializer()(_status);

  factory Project.fromJson(Map<String, dynamic> json) => _$ProjectFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectToJson(this);
}
