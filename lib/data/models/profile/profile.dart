import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile.g.dart';

@JsonSerializable()
class Profile {
  late String firstName;
  late String lastName;
  late String? avatar;

  Profile({
    required this.firstName,
    required this.lastName,
    this.avatar,
  });

  factory Profile.fromJson(Map<String, dynamic> json) => _$ProfileFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileToJson(this);
}
