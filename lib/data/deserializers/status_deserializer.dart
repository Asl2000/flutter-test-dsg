import 'package:flutter_dsg/data/deserializers/deserializer.dart';
import 'package:flutter_dsg/domain/enums/status.dart';

class StatusDeserializer implements Deserializer<Status, String> {
  @override
  Status call(String obj) {
    const dict = {
      'Alive': Status.alive,
      'Dead': Status.dead,
      'unknown': Status.unknown,
    };
    return dict[obj]!;
  }
}
