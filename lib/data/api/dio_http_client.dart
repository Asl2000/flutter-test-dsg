import 'package:dio/dio.dart';
import 'package:flutter_dsg/data/api/api_common.dart';
import 'package:flutter_dsg/data/api/http_client.dart';
import 'package:flutter_dsg/data/api/network_logger.dart';

class DioHttpClient implements HttpClient {
  late Dio _dio;

  DioHttpClient() {
    final dio = Dio(
      BaseOptions(
        baseUrl: ApiCommon.host,
        connectTimeout: const Duration(milliseconds: 10000),
        receiveTimeout: const Duration(milliseconds: 10000),
        sendTimeout: const Duration(milliseconds: 10000),
        responseType: ResponseType.json,
      ),
    );

    _dio = dio;
    dio.interceptors.add(NetworkLogger());
  }

  @override
  Dio get dio => _dio;
}
