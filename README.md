# Test DSG

Приложение сделано в рамках тестового задания

## Resources

- [Api Rick and Morty](https://rickandmortyapi.com/documentation/)
- [Скачать APK](https://gitlab.com/Asl2000/flutter-test-dsg/-/blob/main/build/app/outputs/flutter-apk/app-release.apk)
- [Дизайн](https://www.figma.com/file/fYYwSjzfS4x8BrU9Grvep3/Тестовое-Задание---Кучербаев-Аслан?type=design&node-id=1-6109&mode=design&t=DGw7uvduzI9md6tR-0)

## Stack

- DI (GetIt)
- Auth (Firebase Auth)
- State manager (Redux)
- Rest api (Dio, Firebase)
- Storage (Firebase Storage)

## Функции

- Авторизация и регистрация с помощью номера телефона
- Подтверждение номера телефона с помощью кода смс
- Редактирование данных пользователя
- Редактирование фотографии пользователя
- Просмотр проектов (персонажи Рик и Морти)
